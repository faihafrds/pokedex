import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Pokedex from './Pokedex';

const App = () => {
  return (
    <div className='container'>
      <Pokedex/>
    </div>
  );
}

export default App;
