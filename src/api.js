import axios from 'axios'

const baseURL = `https://pokeapi.co/api/v2`

export const getPokemonList = async (num) => {
  const url = `${baseURL}/pokemon/?offset=${num}&limit=18`
  const res = await axios.get(url)
  return res.data
}

export const getPokemonImg = async (name) => {
  const url = `${baseURL}/pokemon/${name}`
  const res = await axios.get(url)
  return res.data
}

export const getPokemonDetails = async (name) => {
  const url = `${baseURL}/pokemon/${name}`
  const res = await axios.get(url)
  return res.data
}

export const getTypeList = async () => {
  const url = `${baseURL}/type`
  const res = await axios.get(url)
  return res.data
}

export const getPokemonByType = async (id) => {
  const url = `${baseURL}/type/${id}`
  const res = await axios.get(url)
  return res.data
}


