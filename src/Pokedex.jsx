import React, { useState, useEffect } from 'react';
import {Card, CardImg, CardBody, CardTitle, Button, Form, FormGroup, Col, Input, Modal, ModalBody } from 'reactstrap';
import { getPokemonDetails, getPokemonImg, getPokemonList, getTypeList, getPokemonByType } from './api';

const Pokedex = () => {
  const [offset, setOffset] = useState(0);
  const [pokemon, setPokemon] = useState([]);
  const [modal, setModal] = useState(false);
  const [details, setDetails] = useState(null);
  const [type, setType] = useState([]);
  
  useEffect(() => {
    pokemonList(false)
  }, [offset]);

  useEffect(() => {
    typeList()
  }, [])
  
  const pokemonList = async (init) => {
		try {
      if(init){
        setPokemon([])
      }
			const res = await getPokemonList(offset)
			pokemonImg(res.results)
		} catch (error) {
			return error
		}
	}

  const pokemonImg = (result) => {
    try {
      result.forEach(async e => {
        const res = await getPokemonImg(e.name)
        setPokemon(state => [...state, res])
      })
    } catch (error) {
      return error
    }
  }

  const typeList = async () => {
    try {
      const res = await getTypeList()
      console.log(res);
      setType(res.results);
    } catch (error) {
      
    }
  }

  const pokemonByType = async (id) => {
    setPokemon([]);
    const res = await getPokemonByType(id)
		res.pokemon.forEach(async e => {
      const res = await getPokemonImg(e.pokemon.name)
      setPokemon(state => [...state, res])
    })    
  }

  const toggle = async (i) => {
    if(i !== 0){
      const res = await getPokemonDetails(i);
      setDetails(res)
    }
    setModal(!modal)
  };

  return (
    <div>
      <h1 className='mt-3 text-center' id="web-title">Pokedex</h1>
      <div className="row justify-content-center px-1">
        <Form>
          <FormGroup row>
            <Col sm={3}>
              <Input
                id="selectType"
                name="type"
                type="select"
              >
                <option onClick={() => pokemonList(true)}>
                  Select Type
                </option>
                {type !== null && type.map((e, i) => (
                   <option value={e.id} onClick={() => pokemonByType(e.name)}>
                    {e.name}
                 </option>
                ))}
              </Input>
            </Col>
          </FormGroup>
        </Form>
      </div>
      <div className="row justify-content-center px-1">
        {pokemon != null && pokemon.map((e, i) => (
        <div className="col-md-2 col-sm-6">
          <Card
            onClick={()=>toggle(e.id)}
            className='pokedex-card mt-3'
          >
            <CardImg
              alt="pokemon-img"
              src={e.sprites.other.dream_world.front_default}
              top
              className='pt-3 px-2'
              width='100px'
              height='100px'
            />
            <CardBody className='px-0'>
              <CardTitle className='text-center' tag="p" key={i}>
                {e.name}
              </CardTitle>
            </CardBody>
          </Card>
        </div>
        ))}
      </div>
      <div className="row my-3 mx-1 justify-content-center">
        <div className="col-6 text-center">
          <Button
            color="primary"
            onClick={()=>setOffset(offset+20)}
            size='sm'
          >
            Load more
          </Button>
        </div>
      </div>
      {details !== null ? 
      <Modal isOpen={modal} toggle={()=>toggle(0)}>
        <ModalBody>
        <Card>
            <CardImg
              alt="Card image cap"
              src={details !== null ? details.sprites.other.dream_world.front_default  : ''}
              top
              width="180px"
              height="180px"
              className='pt-3'
            />
            <CardBody>
              <CardTitle tag="h5" className='text-center'>
                {details != null ? details.name : ''}
              </CardTitle>
              <ul className='text-center'>
              {details.types !== null ? details.types.map((e)=>(
                <li className='type'>{e.type.name}</li>
              )) : ''}
              </ul>
              <div className="row justify-content-center details-content text-center">
                <div className="col-3">
                  <p className='fs'>Weight</p>
                  <p className='mt-2'>{details !== null ? details.weight : ''} hg</p>
                </div>
                <div className="col-4 center-border">
                  <p className='fs'>Abilities</p>
                  {details !== null ? details.abilities.map((e)=>(
                    <p>{e.ability.name}</p>
                  )): ''}
                </div>
                <div className="col-3">
                  <p className='fs'>Height</p>
                  <p className='mt-2'>{details !== null ? details.height : ''} dm</p>
                </div>
              </div>
              <h5 className='text-center mt-2'>Moves</h5>
              <hr style={{margin: '3px'}}/>
              <ul className='text-center'>
                {details !== null ? details.moves.map((e,i)=>(
                  <>
                  {i <= 19 && 
                  <li className='moves'>{e.move.name},</li>
                  }
                  </>
                )) : ''}
              </ul>
            </CardBody>
          </Card>
        </ModalBody>
      </Modal>
      : ''}
    </div>
  )
}

export default Pokedex