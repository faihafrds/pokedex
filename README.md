# Pokedex Project

## Project Description

This project is a website to explore pokemon. It allows you to see some details of each pokemon and filter them by the type of the pokemon. This project build with ReactJs.

## Demo

[Link Demo](https://pokedex-lilac-one.vercel.app/)

## Development Guide

1. Clone this repo in your computer
2. Install dependencies

```
npm install
```

3. Start Development the App in local

```
npm start
```
